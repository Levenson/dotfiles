#!/usr/local/bin/bash

# Modified from HerbstluftWM's panel.sh 

# COLORS

# Getting colors from `xrdb -query`
if [[ $(uname) == 'FreeBSD' ]]; then
  xrdb=( $(xrdb -query | grep "color[0-9]*:" | sort | cut -f 2-) )
else
  xrdb=( $(xrdb -query | grep -P "color[0-9]*:" | sort | cut -f 2-) )
fi
declare -A color
index=0
for name in black brightgreen brightyellow brightblue brightmagenta brightcyan brightwhite red green yellow blue magenta cyan white grey brightred; do
  color[${name}]=${xrdb[$index]}
  ((index++))
done

bgcolor='#1f1b18'

# GEOMETRY
x=0
y=0
width=1920
height=16

font="Monospace:size=9"
bold="Monospace:style=bold:size=9"

# Using a different font to calculate text width (`textwidth` doesn't work with truetype fonts)
# Neep Semicondensed 11 has the same char width as Pragmata 7pt
calcfont="-xos4-terminus-medium-r-normal--14-140-72-72-c-80-iso8859-15"
# calcfont="-jmk-neep-medium-r-semicondensed--11-100-75-75-c-50-iso8859-9"

# ICONS
# iconpath=${XDG_CONFIG_HOME}/stumpwm/dzen/icons
# icon() {
#   echo -n "^fn(FontAwesome) $1 ^fn($bold)"
# }
iconpath=${XDG_CONFIG_HOME}/stumpwm/dzen/icons
function icon() {
	echo -n "^fg(${color[${2}]})^i(${iconpath}/${1}.xbm)^fg()"
}

# CPU
function temperature() {
  cpu=$(sensors | grep -P "(temp1)|(Core)" | cut -b 16-19)
  echo -n $(icon temp yellow) ${cpu}
}

# MPD
function m() {
  mpc -f %${1}% current | sed 's/ä/ae/g' | sed 's/ö/oe/g' | sed 's/ü/ue/g'
}


while true; do

  echo -n "^fn($bold)$(cat ~/.config/stumpwm/dzen/grouplist.txt) | "

  echo -n "$(cat ~/.config/stumpwm/dzen/windowlist.txt)"
  
  # date_ldn=$(TZ=Europe/London date +"%H:%M")
  date_msk=$(date +"%H:%M")
  date="$(date +%a) $date_msk $(date +%d-%m-%Y)"

  maildir=0
  new=$(ls /mnt/users/abralek/.maildir/new | wc -l | awk '{print $1}')
  if (($new > 0)); then maildir=$new; fi
  
  right="|"
  if (($maildir > 0)); then
    right="${right} $maildir  "
  fi

  right="$right $date  "
  
  # get width of right aligned text.. and add some space..
  width=$(textwidth $calcfont "$right")

  right=""
  if (($maildir > 0)); then
      right="$(icon mail magenta) $maildir"
  fi
  
  right="$right $(icon clock magenta) $date"
  echo "^p(_RIGHT)^p(-$width) $right"

done | dzen2 -h 16 -bg "$bgcolor" -ta l -fg '#efefef'
