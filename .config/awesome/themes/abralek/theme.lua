---------------------------
-- abralek awesome theme --
---------------------------

theme = {}

--
theme.font                          = "Terminus 8"
theme.taglist_font                  = "Entypo 8"
theme.fg_normal                     = "#D7D7D7"
theme.fg_focus                      = "#F6784F"
theme.bg_normal                     = "#060606"
theme.bg_focus                      = "#060606"
theme.fg_urgent                     = "#CC9393"
theme.bg_urgent                     = "#2A1F1E"

theme.border_width                  = "1.5"
theme.border_normal                 = "#000000"
theme.border_focus                  = "#000000"

theme.taglist_fg_focus              = "#F6784F"
theme.taglist_bg_focus              = "#060606"
theme.tasklist_fg_focus             = "#F6784F"
theme.tasklist_bg_focus             = "#060606"
theme.menu_height                   = "16"
theme.menu_width                    = "140"
-- 

-- Display the taglist squares
theme.taglist_squares_sel   = homedir .. "themes/abralek/taglist/squarefw.png"
theme.taglist_squares_unsel = homedir .. "themes/abralek/taglist/squarew.png"

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = homedir .. "themes/abralek/submenu.png"
theme.menu_height = 15
theme.menu_width  = 100

-- You can add as many variables as you wish and access them by using
-- beautiful.variable in your rc.lua

--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = homedir .. "themes/abralek/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = homedir .. "themes/abralek/titlebar/close_focus.png"

theme.titlebar_ontop_button_normal_inactive = homedir .. "themes/abralek/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = homedir .. "themes/abralek/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = homedir .. "themes/abralek/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = homedir .. "themes/abralek/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = homedir .. "themes/abralek/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = homedir .. "themes/abralek/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = homedir .. "themes/abralek/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = homedir .. "themes/abralek/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = homedir .. "themes/abralek/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = homedir .. "themes/abralek/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = homedir .. "themes/abralek/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = homedir .. "themes/abralek/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = homedir .. "themes/abralek/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = homedir .. "themes/abralek/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = homedir .. "themes/abralek/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = homedir .. "themes/abralek/titlebar/maximized_focus_active.png"

theme.wallpaper = homedir .. "themes/abralek/bg.jpg"

-- You can use your own layout icons like this:
theme.layout_fairh = homedir .. "themes/abralek/layouts/fairhw.png"
theme.layout_fairv = homedir .. "themes/abralek/layouts/fairvw.png"
theme.layout_floating  = homedir .. "themes/abralek/layouts/floatingw.png"
theme.layout_magnifier = homedir .. "themes/abralek/layouts/magnifierw.png"
theme.layout_max = homedir .. "themes/abralek/layouts/maxw.png"
theme.layout_fullscreen = homedir .. "themes/abralek/layouts/fullscreenw.png"
theme.layout_tilebottom = homedir .. "themes/abralek/layouts/tilebottomw.png"
theme.layout_tileleft   = homedir .. "themes/abralek/layouts/tileleftw.png"
theme.layout_tile = homedir .. "themes/abralek/layouts/tilew.png"
theme.layout_tiletop = homedir .. "themes/abralek/layouts/tiletopw.png"
theme.layout_spiral  = homedir .. "themes/abralek/layouts/spiralw.png"
theme.layout_dwindle = homedir .. "themes/abralek/layouts/dwindlew.png"

theme.awesome_icon = "/usr/local/share/awesome/icons/awesome16.png"

-- Define the icon theme for application icons. If not set then the icons 
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

theme.tasklist_disable_icon         = true
theme.tasklist_floating             = ""
theme.tasklist_maximized_horizontal = ""
theme.tasklist_maximized_vertical   = ""

return theme
-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
