# -*- mode: sh; -*-

# History stuff
HISTFILE=~/.histfile
HISTSIZE=100000
SAVEHIST=100000


# Variables
LC_ALL=en_US.UTF-8


# Dircolors
LS_COLORS='rs=0:di=01;34:ln=01;36:pi=40;33:so=01;35:do=01;35:bd=40;33;01:cd=40;33;01:or=40;31;01:su=37;41:sg=30;43:tw=30;42:ow=34;42:st=37;44:ex=01;32:';
export LS_COLORS


# Keybindings
bindkey -e


# Alias stuff
alias ls="ls --color"
alias ll="ls -lh"

# moving in dirs
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias .....="cd ../../../.."
alias ......="cd ../../../../.."

# Dir colors
# eval $(dircolors -b $HOME/.dircolors)

# Comp stuff
zmodload zsh/complist 
autoload -Uz compinit
compinit
zstyle :compinstall filename '${HOME}/.zshrc'

zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}

zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:kill:*'   force-list always

zstyle ':completion:*:*:killall:*' menu yes select
zstyle ':completion:*:killall:*'   force-list always

# Functions
sprunge() {
    if [ -z "$1" ]; then
	curl -s -F 'sprunge=<-' http://sprunge.us
    else
	if [ -z "$2" ]; then
            echo -n "$1:"
            cat "$1" | "$0"
	else
            for i in "$@"; do
		"$0" "$i"
            done
	fi
    fi

}

# compress a file or folder
ac()
{
    case "$1" in
        tar.bz2|.tar.bz2) tar cvjf "${2%%/}-$(date +%Y%m%d%h).tar.bz2" "${2%%/}/"  ;;
	tbz2|.tbz2)       tar cvjf "${2%%/}-$(date +%Y%m%d%h).tbz2" "${2%%/}/"     ;;
	tbz|.tbz)         tar cvjf "${2%%/}-$(date +%Y%m%d%h).tbz" "${2%%/}/"      ;;       
	tar.xz)         tar cvJf "${2%%/}-$(date +%Y%m%d%h).tar.gz" "${2%%/}/"      ;;       
	tar.gz|.tar.gz)   tar cvzf "${2%%/}-$(date +%Y%m%d%h).tar.gz" "${2%%/}/"   ;;
	tgz|.tgz)         tar cvjf "${2%%/}-$(date +%Y%m%d%h).tgz" "${2%%/}/"      ;;
	tar|.tar)         tar cvf  "${2%%/}-$(date +%Y%m%d%h).tar" "${2%%/}/"        ;;
        rar|.rar)         rar a "${2}.rar" "$2"            ;;
	zip|.zip)         zip -9 "${2}.zip" "$2"            ;;
	7z|.7z)         7z a "${2}.7z" "$2"            ;;
	lzo|.lzo)         lzop -v "$2"                ;;   
	gz|.gz)         gzip -v "$2"                ;;
	bz2|.bz2)         bzip2 -v "$2"                ;;
	xz|.xz)         xz -v "$2"                    ;; 
	lzma|.lzma)         lzma -v "$2"                ;;  
        *)           echo "ac(): compress a file or directory."
            echo "Usage:   ac <archive type> <filename>"
            echo "Example: ac tar.bz2 PKGBUILD"
            echo "Please specify archive type and source."
            echo "Valid archive types are:"
            echo "tar.bz2, tar.gz, tar.gz, tar, bz2, gz, tbz2, tbz,"
            echo "tgz, lzo, rar, zip, 7z, xz and lzma." ;;
    esac
}

# decompress archive (to directory $2 if wished for and possible)
ad()
{ 
    if [ -f "$1" ] ; then
	case "$1" in
            *.tar.bz2|*.tgz|*.tbz2|*.tbz) mkdir -v "$2" 2>/dev/null ; tar xvjf "$1" -C "$2" ;;
	    *.tar.gz)             mkdir -v "$2" 2>/dev/null ; tar xvzf "$1" -C "$2" ;;
	    *.tar.xz)             mkdir -v "$2" 2>/dev/null ; tar xvJf "$1" ;;
	    *.tar)             mkdir -v "$2" 2>/dev/null ; tar xvf "$1"  -C "$2" ;;
	    *.rar)             mkdir -v "$2" 2>/dev/null ; 7z x   "$1"     "$2" ;;
	    *.zip)             mkdir -v "$2" 2>/dev/null ; unzip   "$1"  -d "$2" ;;
	    *.7z)             mkdir -v "$2" 2>/dev/null ; 7z x    "$1"   -o"$2" ;;
	    *.lzo)             mkdir -v "$2" 2>/dev/null ; lzop -d "$1"   -p"$2" ;;  
	    *.gz)             gunzip "$1"                       ;;
	    *.bz2)             bunzip2 "$1"                       ;;
	    *.Z)                 uncompress "$1"                       ;;
	    *.xz|*.txz|*.lzma|*.tlz)     xz -d "$1"                           ;; 
	    *)               
	esac
    else
        echo "Sorry, '$2' could not be decompressed."
        echo "Usage: ad <archive> <destination>"
        echo "Example: ad PKGBUILD.tar.bz2 ."
        echo "Valid archive types are:"
        echo "tar.bz2, tar.gz, tar.xz, tar, bz2,"
        echo "gz, tbz2, tbz, tgz, lzo,"
        echo "rar, zip, 7z, xz and lzma"
    fi
}

# list content of archive but don't unpack
al()
{
    if [ -f "$1" ]; then
        case "$1" in
	    *.tar.bz2|*.tbz2|*.tbz) tar -jtf "$1"     ;;
	    *.tar.gz)                     tar -ztf "$1"     ;;
	    *.tar|*.tgz|*.tar.xz)                 tar -tf "$1"     ;;
	    *.gz)                 gzip -l "$1"     ;;    
	    *.rar)                 rar vb "$1"     ;;
	    *.zip)                 unzip -l "$1"     ;;
	    *.7z)                 7z l "$1"     ;;
	    *.lzo)                 lzop -l "$1"     ;;  
	    *.xz|*.txz|*.lzma|*.tlz)      xz -l "$1"     ;;
        esac
    else
        echo "Sorry, '$1' is not a valid archive."
	echo "Valid archive types are:"
	echo "tar.bz2, tar.gz, tar.xz, tar, gz,"
	echo "tbz2, tbz, tgz, lzo, rar"
	echo "zip, 7z, xz and lzma"
    fi
}

# Show some status info
status() {
    print
    print "Date..: "$(date "+%Y-%m-%d %H:%M:%S")
    print "Shell.: Zsh $ZSH_VERSION (PID = $$, $SHLVL nests)"
    print "Term..: $TTY ($TERM), ${BAUD:+$BAUD bauds, }$COLUMNS x $LINES chars"
    print "Login.: $LOGNAME (UID = $EUID) on $HOST"
    print "System: $(cat /etc/[A-Za-z]*[_-][rv]e[lr]*)"
    print "Uptime:$(uptime)"
    print
}



# Window title
case $TERM in
    *xterm*|rxvt|rxvt-unicode|rxvt-256color|(dt|k|E)term)
    precmd () { print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~]\a" } 
    preexec () { print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~] ($1)\a" }
    ;;
    screen)
        precmd () { 
            # print -Pn "\e]83;title \"$1\"\a" 
            # print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~]\a" 
	    [[ $a = zsh ]] && print -Pn "\ek$2\e\\" # show the path if no program is running
            [[ $a != zsh ]] && print -Pn "\ek$a\e\\" # if a program is running show that

            # Terminal title
            if [[ -n $STY ]] ; then
                [[ $a = zsh ]] && print -Pn "\e]2;$SHORTHOST:S\[$WINDOW\]:$2\a"
                [[ $a != zsh ]] && print -Pn "\e]2;$SHORTHOST:S\[$WINDOW\]:${a//\%/\%\%}\a"
            elif [[ -n $TMUX ]] ; then
                # We're running in tmux, not screen
                [[ $a = zsh ]] && print -Pn "\e]2;$SHORTHOST:$2\a"
                [[ $a != zsh ]] && print -Pn "\e]2;$SHORTHOST:${a//\%/\%\%}\a"
            fi
        }
        preexec () { 
            # print -Pn "\e]83;title \"$1\"\a" 
            # print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~] ($1)\a" 
        }
        ;; 
esac


# Prompt
setprompt () {
    # load some modules
    autoload -U zsh/terminfo # Used in the colour alias below
    # Use colorized output, necessary for prompts and completions.
    autoload -U colors && colors
    setopt prompt_subst

    # make some aliases for the colours: (coud use normal escap.seq's too)
    for color in RED GREEN YELLOW BLUE MAGENTA CYAN WHITE; do
        eval PR_$color='%{$fg[${(L)color}]%}'
    done
    PR_NO_COLOR="%{$terminfo[sgr0]%}"

    # Check the UID
    if [[ $UID -ge 1000 ]]; then # normal user
        eval PR_USER='%n'
        eval PR_USER_OP='%#'
    elif [[ $UID -eq 0 ]]; then # root
        eval PR_USER='${PR_RED}%n${PR_NO_COLOR}'
        eval PR_USER_OP='${PR_RED}%#${PR_NO_COLOR}'
    fi      

    # Check if we are on SSH or not  --{FIXME}--  always goes to |no SSH|
    if [[ -z "$SSH_CLIENT"  ||  -z "$SSH2_CLIENT" ]]; then 
        eval PR_HOST='%M' # no SSH
    else 
        eval PR_HOST='${PR_YELLOW}%M${PR_NO_COLOR}' #SSH
    fi
    if [[ -f /inchroot ]]; then
        eval PR_HOST='${PR_YELLOW}%M${PR_NO_COLOR}' #SSH
    fi
    # set the prompt
    PROMPT=$'%B[${PR_USER}@${PR_HOST}]%#%b '    # default prompt
    RPROMPT=$'%B %~%b'				# prompt for right side of screen
}

setprompt
