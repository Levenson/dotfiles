#!/usr/bin/env python3.4

import re
import os
import sys


def get_password(machine, login, port):
    """A docstring."""
    s = "\s*".join([
        "machine", machine,
        "login", login,
        "port", port,
        "password", "([^ ]*)\n"
    ])
    for line in os.popen("gpg2 -q --for-your-eyes-only --no-tty -d ~/.authinfo.gpg"):
        match = re.match(s, line)
        if match:
            return match.group(1)

if __name__ == "__main__":
    _, machine, login, port, *_ = sys.argv
    print(get_password(machine, login, port))
